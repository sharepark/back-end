from django.contrib import admin

from .models import Place, Spec

class PlaceAdmin(admin.ModelAdmin):
    model = Place

    def name(self, obj):
        return obj.formatted_address

class SpecAdmin(admin.ModelAdmin):
    model = Spec

    def name(self, obj):
        return obj.spec + ' ' + obj.place

admin.site.register(Place, PlaceAdmin)
admin.site.register(Spec, SpecAdmin)
