from django.conf import settings
from django.db import models

class Place(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    TYPES = [
        ('STPL', 'Staanplaats Buiten'),
        ('STPL+', 'Staanplaats Overdekt'),
        ('GGE', 'Alleenstaande Garage'),
        ('OG-STPL', 'Gedeelde garage, staanplaats'),
        ('OG-GGE', 'Gedeelde garage, garagebox'),
    ]
    type = models.CharField(max_length=8, choices=TYPES)

    AVAIL = [
        ('AVAIL', 'Beschikbaar'),
        ('UNAVAIL', 'Tijdelijk onbeschikbaar'),
        ('WEEKEND', 'Enkel weekends'),
        ('CUSTOM', 'Zelf in te vullen data'),
    ]
    availability = models.CharField(max_length=8, choices=AVAIL)

    HIRING_OPTS = [
        ('EVERYONE', 'Iedereen'),
        ('NEIGHBOR', 'Mede-bewoners (privé garage)'),
        ('INVITE', 'Manueel toestaan/weigeren'),
    ]
    hiring = models.CharField(max_length=8, choices=HIRING_OPTS)

    formatted_address = models.CharField(max_length=250, null=True)
    address = models.CharField(max_length=100)
    locality = models.CharField(max_length=50)
    postalCode = models.CharField(max_length=10)
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    photo = models.ImageField(blank=True, null=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.address + " @" + self.user.username)
    
class Spec(models.Model):
    place = models.ForeignKey(Place, on_delete=models.CASCADE, related_name='specs')
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=50)
