from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from users.models import SPUser
from .models import Place, Spec

class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=150, validators=
        [UniqueValidator(queryset=SPUser.objects.all(), lookup='iexact')])
    first_name = serializers.CharField(max_length=150)
    last_name = serializers.CharField(max_length=150)
    email = serializers.EmailField(validators=
        [UniqueValidator(queryset=SPUser.objects.all(), lookup='iexact')])

    def create(self, validated_data):
        validated_data["password"] = ""
        validated_data["is_active"] = False
        validated_data["username"] = validated_data["username"].lower()
        validated_data["email"] = validated_data["email"].lower()
        user = SPUser.objects.create(**validated_data)
        return user

    class Meta:
        model = SPUser
        fields = ('pk', 'username', 'first_name', 'last_name', 'email',)

class PublicUserSerializer(serializers.ModelSerializer):
    rating_score = serializers.SerializerMethodField(read_only=True)
    rating_amount = serializers.SerializerMethodField(read_only=True)

    def get_rating_score(self, user):
        if user.reviews:
            score = 0
            count = 0
            for review in user.reviews.all():
                count += 1
                score += review.rating
        if count != 0:
            return score / count
        return -1

    def get_rating_amount(self, user):
        if user.reviews:
            count = 0
            for _ in user.reviews.all():
                count += 1
            return count
        return -1

    class Meta:
        model = SPUser
        fields = ('pk', 'username', 'first_name', 'last_name', 'rating_score', 'rating_amount',)

class SpecSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spec
        fields = ('pk', 'name', 'value',)

class PlaceSerializer(serializers.ModelSerializer):
    user = PublicUserSerializer(read_only=True)
    specs = SpecSerializer(read_only=True, many=True)

    class Meta:
        model = Place
        fields = ('id', 'type', 'availability', 'hiring', 'formatted_address', 'address', 'locality', 'postalCode', 'latitude', 'longitude', 'user', 'photo', 'description', 'specs',)
