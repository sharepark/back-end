from django.urls import path, include

from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

from .views import user_detail, create_user, forgot_password, activate_api, reset_api, PlaceViewSet

router = routers.DefaultRouter()
router.register(r'places', PlaceViewSet)

urlpatterns = [
    path('activate/<str:uid>/<str:token>/', activate_api),
    path('reset/<str:uid>/<str:token>/', reset_api),

    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),

    path('reset/send/', forgot_password, name='send_reset'),
    path('user/new/', create_user),
    path('user/', user_detail),

    path('', include(router.urls))
]
