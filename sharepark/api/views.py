from django.http import JsonResponse
from django.core import mail
from django.template.loader import render_to_string
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils.html import strip_tags
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings

from rest_framework import status, viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny

from users.models import SPUser

from .models import Place
from .serializers import UserSerializer, PlaceSerializer

class PlaceViewSet(viewsets.ModelViewSet):
    permission_classes = [AllowAny]

    queryset = Place.objects.all()
    serializer_class = PlaceSerializer

@api_view(['GET'])
@csrf_exempt
def user_detail(request):
    if request.method == 'GET':
        serializer = UserSerializer(request.user)
        return Response(serializer.data)
    return JsonResponse({}, status=403)

@api_view(['POST'])
@permission_classes([])
def create_user(request):
    data = JSONParser().parse(request)
    serializer = UserSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        html_message = render_to_string('welcome_email.html',
            {'name': serializer.data["first_name"], 'email': serializer.data["email"],
             'uid': urlsafe_base64_encode(force_bytes(serializer.data["pk"])),
             'token': default_token_generator.make_token(
                 SPUser.objects.get(pk=serializer.data["pk"]))})
        mail.send_mail('Welcome to SharePark!',
            strip_tags(html_message),
            settings.EMAIL_FROM,
            [serializer.data["email"]],
            html_message=html_message)
        return JsonResponse(serializer.data, status=201)
    return JsonResponse(serializer.errors, status=400)

@api_view(['POST'])
@permission_classes([])
def forgot_password(request):
    if 'username' in request.data:
        welcome_template = 'welcome_email.html'
        welcome_subject = 'Welcome to SharePark!'
        forgot_template = 'forgot_email.html'
        forgot_subject = 'Reset your account password'

        if SPUser.objects.filter(username=request.data['username'].lower()).exists():
            user = SPUser.objects.get(username=request.data['username'].lower())
            if user.is_active:
                html_message = render_to_string(forgot_template,
                        {'name': user.first_name, 'email': user.email,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'token': default_token_generator.make_token(user)})
                mail.send_mail(forgot_subject,
                        strip_tags(html_message),
                        settings.EMAIL_FROM,
                        [user.email],
                        html_message=html_message)
            else:
                html_message = render_to_string(welcome_template,
                    {'name': user.first_name, 'email': user.email,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'token': default_token_generator.make_token(user)})
                mail.send_mail(welcome_subject,
                        strip_tags(html_message),
                        settings.EMAIL_FROM,
                        [user.email],
                        html_message=html_message)
        elif SPUser.objects.filter(email=request.data['username'].lower()).exists():
            user = SPUser.objects.get(email=request.data['username'].lower())
            if user.is_active:
                html_message = render_to_string(forgot_template,
                        {'name': user.first_name, 'email': user.email,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'token': default_token_generator.make_token(user)})
                mail.send_mail(forgot_subject,
                        strip_tags(html_message),
                        settings.EMAIL_FROM,
                        [user.email],
                        html_message=html_message)
            else:
                html_message = render_to_string(welcome_template,
                    {'name': user.first_name, 'email': user.email,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'token': default_token_generator.make_token(user)})
                mail.send_mail(welcome_subject,
                        strip_tags(html_message),
                        settings.EMAIL_FROM,
                        [user.email],
                        html_message=html_message)
        return Response(
                {'detail': 'If an account exists, you\'ll receive a reset email in a moment.'},
                status=status.HTTP_200_OK)
    return Response({'detail': 'Invalid request'}, status=status.HTTP_400_BAD_REQUEST)

def reset_password(request, uid, token):
    try:
        priv_key = force_text(urlsafe_base64_decode(uid))
        user = SPUser.objects.get(pk=priv_key)
    except(TypeError, ValueError, OverflowError, SPUser.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        if request.POST and 'password' in request.POST:
            user.set_password(request.POST['password'])
            user.save()
            html_message = render_to_string('reset_email.html',
                    {'name': user.first_name, 'email': user.email})
            mail.send_mail('Your account password was reset',
                    strip_tags(html_message),
                    settings.EMAIL_FROM,
                    [user.email],
                    html_message=html_message)
            return redirect('reset_success')
        return render(request, 'reset_password.html')
    return render(request, 'reset_expired.html')

def reset_success(request):
    return render(request, 'reset_success.html')

def activate(request, uid, token):
    try:
        priv_key = force_text(urlsafe_base64_decode(uid))
        user = SPUser.objects.get(pk=priv_key)
    except(TypeError, ValueError, OverflowError, SPUser.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        if request.POST and 'password' in request.POST:
            user.set_password(request.POST['password'])
            user.save()
            return redirect('activation_complete')
        return render(request, 'set_password.html')
    return render(request, 'setup_invalid.html')

def activation_complete(request):
    return render(request, 'account_setup.html')

@api_view(['GET', 'POST'])
@csrf_exempt
@permission_classes([])
def activate_api(request, uid, token):
    try:
        priv_key = force_text(urlsafe_base64_decode(uid))
        user = SPUser.objects.get(pk=priv_key)
    except(TypeError, ValueError, OverflowError, SPUser.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        if 'password' in request.data:
            user.set_password(request.data['password'])
            user.save()
            return JsonResponse({'status': 'password_set'}, status=200)
        return JsonResponse({'status': 'set_password'}, status=200)
    return JsonResponse({'status': 'link_expired'}, status=403)

@api_view(['GET', 'POST'])
@csrf_exempt
@permission_classes([])
def reset_api(request, uid, token):
    try:
        priv_key = force_text(urlsafe_base64_decode(uid))
        user = SPUser.objects.get(pk=priv_key)
    except(TypeError, ValueError, OverflowError, SPUser.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        if 'password' in request.data:
            user.set_password(request.data['password'])
            user.save()
            html_message = render_to_string('reset_email.html',
                    {'name': user.first_name, 'email': user.email})
            mail.send_mail('Your account password was reset',
                    strip_tags(html_message),
                    settings.EMAIL_FROM,
                    [user.email],
                    html_message=html_message)
            return JsonResponse({'status': 'reset_success'}, status=200)
        return JsonResponse({'status': 'set_password'}, status=200)
    return JsonResponse({'status': 'link_expired'}, status=403)
