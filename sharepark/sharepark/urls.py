"""sharepark URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from api.views import reset_password, reset_success
from api.views import activate, activation_complete

admin.site.site_header = 'SharePark Admin'
admin.site.index_title = 'Beheer app'
admin.site.site_title = 'SharePark'

urlpatterns = [
    path('reset/success/', reset_success, name='reset_success'),
    path('reset/<str:uid>/<str:token>/', reset_password),

    path('activate/complete/', activation_complete, name='activation_complete'),
    path('activate/<str:uid>/<str:token>/', activate),

    path('api/', include('api.urls')),
    path('backend/', admin.site.urls),
]
