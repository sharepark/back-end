from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import SPUserCreationForm, SPUserChangeForm
from .models import SPUser, Review

class SPUserAdmin(UserAdmin):
    model = SPUser
    list_display = ['email', 'username', 'name', 'is_active']

    def name(self, obj):
        return obj.first_name + " " + obj.last_name

admin.site.register(SPUser, SPUserAdmin)
admin.site.register(Review)