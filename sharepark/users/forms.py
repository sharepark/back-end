from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import SPUser

class SPUserCreationForm(UserCreationForm):

    class Meta:
        model = SPUser
        fields = ('username', 'email', 'first_name', 'last_name')

class SPUserChangeForm(UserChangeForm):

    class Meta:
        model = SPUser
        fields = ('username', 'email', 'first_name', 'last_name')
