from django.db import models
from django.contrib.auth.models import AbstractUser

class SPUser(AbstractUser):
    email = models.EmailField(unique=True)

    def __str__(self):
        return str(self.username)

class Review(models.Model):
    user = models.ForeignKey(SPUser, on_delete=models.CASCADE, related_name='reviews')
    rating = models.DecimalField(max_digits=1, decimal_places=0, default=0)
    by_user = models.ForeignKey(SPUser, on_delete=models.SET_NULL, null=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return str("Rating for @" + self.user.username + " by @" + self.by_user.username)